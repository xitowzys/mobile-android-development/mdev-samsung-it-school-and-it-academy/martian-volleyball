import kotlin.math.*

fun main() {

    var n = 0

    val (k, x, y) = readLine()!!.split(' ').map(String::toInt)
    val maxScore = max(x, y)

    when {
        k < abs(x) || k < abs(y) -> n = 0

        (abs(x - y)) > 1 -> n = k - maxScore

        (abs(x - y)) == 1 -> when {
            maxScore >= k -> n = 1
            maxScore < k -> n = k - maxScore
        }

        abs(x) == abs(y) -> when {
            k - maxScore <= 1 -> n = 2
            k - maxScore > 1 -> n = k - maxScore
        }
    }

    println(n)
}